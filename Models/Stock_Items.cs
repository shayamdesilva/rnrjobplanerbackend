﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWebApi.Models
{
    
    public class Stock_Items
    {
        [Key]
        public String StockCode { get; set; }
        public string? Description { get; set; }
        public double? LatestCost { get; set; }
        public double? AveCost { get; set; }
    }
}
