﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWebApi.Models
{
    public class Prod_Jobcard
    {
        public int Id { get; set; }
        public int SalesId { get; set; }
        public int SalesLine { get; set; }
        public String StockCode { get; set; }
        public String? Note { get; set; }
        public String? AddtionalNotes { get; set; }
        public bool? MakeToStock { get; set; }
        public bool? FinSilver { get; set; }
        public bool? FinFlientGrey { get; set; }
        public bool? FinMatBlack { get; set; }
        public bool? FinPickle { get; set; }
        public bool? FinNoPaint { get; set; }
        public bool? FinPaintWeldsOnly { get; set; }
        public bool? FinPolishWelds { get; set; }
        public bool? EnableAddNote { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? ProductionStartDate { get; set; }
        public String? Status { get; set; }
        public String? InStage { get; set; }
        public float OrderQTY { get; set; }
        public float? Sales { get; set; }
        public float? Cost { get; set; }
        public float? Gp { get; set; }
        public DateTime? ProductionComplete { get; set; }
        public DateTime? PlanProductionDate { get; set; }
        public String? Drawing { get; set; }
        public String? Division { get; set; }
        public String? PrintBy { get; set; }
        public bool? Active { get; set; }
        public String? Color { get; set; }
    }
}
