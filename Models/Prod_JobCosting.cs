﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWebApi.Models
{
    public class Prod_JobCosting
    {
        [Key]
        public int RecId { get; set; }
        public Guid? RefUID { get; set; }
        public String? ItemCode { get; set; }
        public float? Qty { get; set; }
        public float? UnitCost { get; set; }
        public float? LineAmount { get; set; }
        public float? AvgCost { get; set; }
        public float? LatestCost { get; set; }
        public String? Description { get; set; }
        public DateTime? TransDate { get; set; }
        public bool? Active { get; set; }


    }
}
