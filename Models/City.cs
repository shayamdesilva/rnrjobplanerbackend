﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWenApi.Models
{
    public class City
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}
