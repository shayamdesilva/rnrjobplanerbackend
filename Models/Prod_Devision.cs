﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWebApi.Models
{
    public class Prod_Devision
    {
        public int Id { get; set; }
        public String DevisionId { get; set; }
        public String Name { get; set; }
    }
}
