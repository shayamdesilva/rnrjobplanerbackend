﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWebApi.Models
{
    public class ProdRoute
    {
        public int SoSeqNo { get; set; }
        public String? stockCode { get; set; }
        public String? opCode { get; set; }
        public String? assignTo { get; set; }
        public bool? enable { get; set; }
        public int? salesLineref { get; set; }
        public String? nextOperation { get; set; }
        public float? estimatedTime { get; set; }
        public float? actualHours { get; set; }
        public String? status { get; set; }
        public Guid id { get; set; }
        public DateTime? orderDate { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? startDate { get; set; }
        public float? orderQty { get; set; }
        public float? productionQty { get; set; }
        public float? scrapQty { get; set; }
        public DateTime? completeDate { get; set; }
        public bool? active { get; set; }

    }
}
