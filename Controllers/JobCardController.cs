﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RnRWebApi.Models;
using RnRWenApi.DataContext;

namespace RnRWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobCardController : ControllerBase
    {

        readonly DatabaseContext databaseContext;

        public JobCardController(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        [HttpGet]
        public IActionResult getJobCardData()
        {
            try
            {
                var jobCardData = databaseContext.PROD_JOBCARD.ToList();
                return Ok(JsonConvert.SerializeObject(jobCardData));
            }catch(Exception e)
            {
                throw e;
            }

        }

        [HttpPost("add")]//http://localhost:50388/api/jobcard/add?note=My Note
        public async Task<IActionResult> addJobCardData(String note)
        {
            Prod_Jobcard jobcard = new Prod_Jobcard();
            jobcard.Note = note;
            await databaseContext.PROD_JOBCARD.AddAsync(jobcard);
            await databaseContext.SaveChangesAsync();
            return Ok(jobcard);
        }

        [HttpPost("post")]//http://localhost:50388/api/jobcard/post
        public async Task<IActionResult> addJobCard(Prod_Jobcard jobcard)
        {
            //Prod_Jobcard jobcard = new Prod_Jobcard();
            //jobcard.Note = card;
            await databaseContext.PROD_JOBCARD.AddAsync(jobcard);
            await databaseContext.SaveChangesAsync();
            return Ok(jobcard);
        }


        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> addJobCard(int id)
        {
            var jobCard = await databaseContext.PROD_JOBCARD.FindAsync(id);
            databaseContext.PROD_JOBCARD.Remove(jobCard);
            await databaseContext.SaveChangesAsync();
            return Ok(id);
        }

        [HttpGet("get/{id}")]
        public IActionResult getJobCardDataById(int id)
        {
            try
            {
                var jobCardData = databaseContext.PROD_JOBCARD.FindAsync(id);
                return Ok(JsonConvert.SerializeObject(jobCardData));
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
