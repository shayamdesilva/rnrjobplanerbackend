﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RnRWenApi.DataContext;

namespace RnRWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaffController : ControllerBase
    {

        readonly DatabaseContext databaseContext;

        public StaffController(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        [HttpGet]
        public IActionResult getStaffData()
        {
            try
            {
                var staffData = databaseContext.PROD_STAFF.ToList();
                return Ok(staffData);
            }catch(Exception e)
            {
                var x = e;
                throw e;
            }

        }
    }
}
