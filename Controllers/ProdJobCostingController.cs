﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RnRWebApi.Models;
using RnRWenApi.DataContext;

namespace RnRWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdJobCostingController : ControllerBase
    {

        readonly DatabaseContext databaseContext;

        public ProdJobCostingController(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        [HttpGet]
        public IActionResult getJobCostingData()
        {
            try
            {
                var jobCosting = databaseContext.PROD_JOBCOSTING.ToList();
                return Ok(jobCosting);
            }
            catch (Exception e)
            {
                var x = e;
                throw e;
            }

        }

        [HttpPost("addProdJob")]
        public IActionResult getJobCostingData1(List<Prod_JobCosting> jobCost)
        {
            try
            {
                foreach (Prod_JobCosting job in jobCost)
                {
                    if (job.RecId > 0)
                    {
                        Prod_JobCosting prod_JobCosting = databaseContext.PROD_JOBCOSTING.Where(s => s.RecId == job.RecId).FirstOrDefault();
                        if (job.Qty == null)
                        {
                            prod_JobCosting.Qty = 0;
                        }
                        else
                        {
                            prod_JobCosting.Qty = float.Parse(job.Qty.ToString());
                        }

                        prod_JobCosting.Description = job.Description != null ? job.Description : null;

                        if (job.UnitCost == null)
                        {
                            prod_JobCosting.UnitCost = 0;
                        }
                        else
                        {
                            prod_JobCosting.UnitCost = float.Parse(job.UnitCost.ToString());
                        }

                        if (job.LineAmount == null)
                        {
                            prod_JobCosting.LineAmount = 0;
                        }
                        else
                        {
                            prod_JobCosting.LineAmount = float.Parse(job.LineAmount.ToString());
                        }


                        prod_JobCosting.TransDate = job.TransDate;

                        if (job.Active == null)
                        {
                            prod_JobCosting.Active = false;
                        }
                        else
                        {
                            prod_JobCosting.Active = job.Active;
                        }

                        if (job.AvgCost == null)
                        {
                            prod_JobCosting.AvgCost = 0;
                        }
                        else
                        {
                            prod_JobCosting.AvgCost = float.Parse(job.AvgCost.ToString());
                        }



                        prod_JobCosting.ItemCode = job.ItemCode;
                        databaseContext.SaveChanges();

                    }


                }
                    var jobCosting = databaseContext.PROD_JOBCOSTING.ToList();
                return Ok(jobCosting);
            }
            catch (Exception e)
            {
                var x = e;
                throw e;
            }

        }

        //[HttpPost("addProdJob")]
        //public  IActionResult addJobCostingDate(List<Prod_JobCosting> jobCosting)
        //{
        //    //foreach (Prod_JobCosting job in jobCosting)
        //    //{
        //    //    Console.WriteLine(job.Description);

        //    //}


        //    //return Ok(jobCosting);
        //    Prod_JobCosting prod_JobCosting = null;
        //    foreach (Prod_JobCosting job in jobCosting)
        //    {
                
        //        if (job.RecId > 0)
        //        {
        //           // prod_JobCosting = databaseContext.PROD_JOBCOSTING.Where(s => s.RecId == job.RecId).FirstOrDefault();
        //            //var x = 10;
        //            // prod_JobCosting.RefUID = job.RefUID;
        //            /prod_JobCosting.UnitCost = job.UnitCost;
        //            //prod_JobCosting.LineAmount = job.LineAmount;
        //            //prod_JobCosting.LineAmount = job.LineAmount;
        //            //    prod_JobCosting.TransDate = job.TransDate;
        //            //    prod_JobCosting.Active = job.Active;
        //            //    prod_JobCosting.AvgCost = job.AvgCost;
        //            //prod_JobCosting.Description = job.Description;
        //            //    prod_JobCosting.ItemCode = job.ItemCode;
        //            //    await databaseContext.PROD_JOBCOSTING.AddAsync(prod_JobCosting);
        //            //    await databaseContext.SaveChangesAsync();
        //            var j = 20;
        //        }
        //        //else
        //        //{
        //        //    prod_JobCosting = new Prod_JobCosting();
        //        //    prod_JobCosting.RefUID = job.RefUID;
        //        //    prod_JobCosting.Qty = job.Qty;
        //        //    prod_JobCosting.UnitCost = job.UnitCost;
        //        //    prod_JobCosting.LineAmount = job.LineAmount;
        //        //    prod_JobCosting.LineAmount = job.LineAmount;
        //        //    prod_JobCosting.TransDate = job.TransDate;
        //        //    prod_JobCosting.Active = job.Active;
        //        //    prod_JobCosting.AvgCost = job.AvgCost;
        //        //    prod_JobCosting.Description = job.Description;
        //        //    prod_JobCosting.ItemCode = job.ItemCode;
        //        //    await databaseContext.PROD_JOBCOSTING.AddAsync(prod_JobCosting);
        //        //    await databaseContext.SaveChangesAsync();
        //        //}
        //    }
        //    return Ok(prod_JobCosting);


        //}


        [HttpPost("addProdJobs/{jobCosting}")]
        public async Task<IActionResult> addJobCosting(String jobCosting)
        {
            //foreach (Prod_JobCosting job in jobCosting)
            //{
                Console.WriteLine(jobCosting);

            //}


            //return Ok(jobCosting);
            Prod_JobCosting prod_JobCosting = null;
           
            return Ok(prod_JobCosting);


        }

        //[HttpPost("add")]//http://localhost:50388/api/jobcard/add?note=My Note
        //public async Task<IActionResult> addJobCardData(String note)
        //{
        //    Prod_Jobcard jobcard = new Prod_Jobcard();
        //    jobcard.Note = note;
        //    await databaseContext.PROD_JOBCARD.AddAsync(jobcard);
        //    await databaseContext.SaveChangesAsync();
        //    return Ok(jobcard);
        //}

        //[HttpPost("post")]//http://localhost:50388/api/jobcard/post
        //public async Task<IActionResult> addJobCard(Prod_Jobcard jobcard)
        //{
        //    //Prod_Jobcard jobcard = new Prod_Jobcard();
        //    //jobcard.Note = card;
        //    await databaseContext.PROD_JOBCARD.AddAsync(jobcard);
        //    await databaseContext.SaveChangesAsync();
        //    return Ok(jobcard);
        //}

    }


}
