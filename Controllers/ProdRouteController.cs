﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RnRWebApi.Models;
using RnRWenApi.DataContext;

namespace RnRWenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdRouteController : ControllerBase
    {
        private readonly DatabaseContext dataContext;

        public ProdRouteController(DatabaseContext dataContext)
        {
            this.dataContext = dataContext;
        }

        [HttpGet]
        public IActionResult GetRoute()
        {
            try
            {
                var prodRoute = dataContext.PROD_ROUTE.ToList();
                return Ok(prodRoute);

            }catch(Exception e)
            {
                var x = 1;
                throw e;
            }

        }

        //[HttpPut("update/{id}")]
        //public async Task<IActionResult> updateRouteById(int id)
        //{

        //    ProdRoute route = new ProdRoute();
        //    route.status = "Start";
        //    await dataContext.AddAsync(route);
        //    await dataContext.SaveChangesAsync();
        //    return Ok(route);



        //}

        [HttpPost("updateroutestartstatus/{id}")]
        public async Task<IActionResult> updateRouteByroute(Guid id)
        {

            var std = await dataContext.PROD_ROUTE.FindAsync(id);
            std.status = "Start";
            dataContext.SaveChanges();
            return Ok(std);
        }

        [HttpPost("updaterouteendstatus/{id}")]
        public async Task<IActionResult> updateEndRouteByroute(Guid id)
        {

            var std = await dataContext.PROD_ROUTE.FindAsync(id);
            std.status = "End";
            dataContext.SaveChanges();
            return Ok(std);
        }
    }
}
