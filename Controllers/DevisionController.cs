﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RnRWenApi.DataContext;

namespace RnRWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevisionController : ControllerBase
    {

        readonly DatabaseContext databaseContext;

        public DevisionController(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        [HttpGet]
        public IActionResult getDevisionData()
        {
            try
            {
                var devisionData = databaseContext.PROD_DIVISIONS.ToList();
                return Ok(devisionData);
            }
            catch (Exception e)
            {
                var x = e;
                throw e;
            }

        }
    }


}
