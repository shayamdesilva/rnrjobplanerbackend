﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RnRWenApi.DataContext;

namespace RnRWenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockItemController : ControllerBase
    {
        private readonly DatabaseContext dataContext;

        public StockItemController(DatabaseContext dataContext)
        {
            this.dataContext = dataContext;
        }

        [HttpGet]
        public IActionResult getItems()
        {
                var stockItem =  dataContext.STOCK_ITEMS.ToList();
                return Ok(stockItem);


        }
    }
}
