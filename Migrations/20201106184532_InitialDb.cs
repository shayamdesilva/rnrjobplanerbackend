﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RnRWebApi.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PROD_JOBCARD",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SalesLine = table.Column<int>(nullable: false),
                    StockCode = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    AddtionalNotes = table.Column<string>(nullable: true),
                    MakeToStock = table.Column<int>(nullable: false),
                    FinSilver = table.Column<int>(nullable: false),
                    FinFlientGrey = table.Column<int>(nullable: false),
                    FinMatBlack = table.Column<int>(nullable: false),
                    FinPickle = table.Column<int>(nullable: false),
                    FinNoPaint = table.Column<int>(nullable: false),
                    FinPaintWeldsOnly = table.Column<int>(nullable: false),
                    FinPolishWelds = table.Column<int>(nullable: false),
                    EnableAddNote = table.Column<int>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    DueDate = table.Column<DateTime>(nullable: false),
                    ProductionStartDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    InStage = table.Column<string>(nullable: true),
                    OrderQTY = table.Column<double>(nullable: false),
                    Sales = table.Column<double>(nullable: false),
                    Cost = table.Column<double>(nullable: false),
                    Gp = table.Column<double>(nullable: false),
                    ProductionComplete = table.Column<DateTime>(nullable: false),
                    PlanProductionDate = table.Column<DateTime>(nullable: false),
                    Drawing = table.Column<string>(nullable: true),
                    Division = table.Column<string>(nullable: true),
                    PrintBy = table.Column<string>(nullable: true),
                    Active = table.Column<int>(nullable: false),
                    Color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PROD_JOBCARD", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PROD_STAFF",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PROD_STAFF", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    uaserName = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "PROD_JOBCARD");

            migrationBuilder.DropTable(
                name: "PROD_STAFF");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
