﻿using Microsoft.EntityFrameworkCore;
using RnRWebApi.Models;
using RnRWenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnRWenApi.DataContext
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<City> City { get; set; }

        public DbSet<Users> Users { get; set; }

        public DbSet<Prod_Staff> PROD_STAFF { get; set; }

        public DbSet<Prod_Jobcard> PROD_JOBCARD { get; set; }
        public DbSet<Prod_Devision> PROD_DIVISIONS { get; set; }
        public DbSet<ProdRoute> PROD_ROUTE { get; set; }
        public DbSet<Stock_Items> STOCK_ITEMS { get; set; }
        public DbSet<Prod_JobCosting> PROD_JOBCOSTING { get; set; }


    }
}
